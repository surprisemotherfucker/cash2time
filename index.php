<?php
require __DIR__.'/vendor/autoload.php';

$app = new \Slim\Slim();

$app->get('/', function () {
    try {
        $data = "TEST";
        echo json_encode(array('result' => $data, 'success' => true, 'code'=>'0'));
    } catch (Exception $e) {
        echo json_encode(array('success' => false,'code'=>array( 'code' => 400, 'msg'=>$e->getMessage())));
    }
});

//products by $id
$app->get('/products/id/:id', function ($id) {
    try {
        $id=explode(',',$id);
        $data=Product::whereIn('id',$id)->get();
        echo json_encode(array('result' => $data, 'success' => true, 'code'=>'0'));
    } catch (Exception $e) {
        echo json_encode(array('success' => false,'code'=>array( 'code' => 400, 'msg'=>$e->getMessage())));
    }
});

//
function iter($tag,$price,$places,$categories){
    $places=explode(',',$places);
    $categories=explode(',',$categories);
    $place = Place::whereNotIn('id',$places)->where('tag','like',"%".$tag."%");
    if($place->count() <1)
    {
        return array('msg'=>"404");
    }
    $place = $place->get(array('id'));
    $id_str="";
    foreach ($place as $id)
    {
        $id=preg_replace('/[^0-9]/', '', $id);;
        $id_str=$id_str.$id.",";
    }
    $id_str=substr($id_str,0,-1);
    $id_arr=explode(',',$id_str);
    $data = Product::whereNotIn('category',$categories)->whereIn('parent',$id_arr)->where('priority','1')->where('price','<=',$price);
    if($data->count() <1)
    {
        return array('msg'=>"404");
    }
    $data=$data->orderByRaw('RAND()')->first();
    $parent=$data->parent;
    $category=$data->category;
    $id=$data->id;
    $max=$data->price;
    $return[]=$data;
    $balance=$price-$max;
    if($balance> 150)
    {
            $data1=Product::where('parent',$parent)->where('id','!=',$id)->where('priority','2')->where('price','<=',$balance);

            $data1=$data1->orderByRaw('RAND()')->first();
            if($data1!=null)
            {
                $return[]=$data1;
                $balance=$balance-$data1->price;
                return array('return'=>$return,'place'=>$parent,'balance'=>$balance,'category'=>$category);
            }
            else{
                return array('return'=>array($data),'place'=>$parent,'balance'=>$balance,'category'=>$category);
            }

    }
    return array('return'=>array($data),'place'=>$parent,'balance'=>$balance,'category'=>$category);
}

// random_free
function random_free($tag){
    $place = Place::where('tag','like',"%".$tag."%")->get(array('id'));
    $id_str="";
    foreach ($place as $id)
    {
        $id=preg_replace('/[^0-9]/', '', $id);;
        $id_str=$id_str.$id.",";
    }
    $id_str=substr($id_str,0,-1);
    $id_arr=explode(',',$id_str);
    $data = Product::whereIn('parent',  $id_arr)->where('price',0)->orderByRaw('RAND()')->first();

    return array('result'=>$data);
}

function tag_cards($tag,$price) {
    try {
        if($tag=='party')
        {
            if($price<=300)
                $z=1.0;
            else
                $z=0.9;
        }elseif($tag=='extreme')
        {
                $z=0.8;
        }
        else{
            $z=0.6;
        }
        for ($k = 1.0;$k >= $z ; $k-=0.1) {
            $card=null;
            $balance=$price;
            $places="";
            $categories="";
            $count=0;
            $number=0;

            while($balance>200) {
                if($tag!='party')
                {
                    $iter_price = $balance * $k;
                }
                else {
                    $iter_price = $balance;
                }
                $iter = iter($tag, $iter_price,$places,$categories);
                if($iter['msg']=="404" && $iter_price == $balance)
                {
                    break;
                }
                if($iter['return']!=null) {
                    if($iter['return'][1]!=null)
                    {
                        $card['prod_'.$number] = $iter['return'][0];
                        $number++;
                        $card['prod_'.$number] = $iter['return'][1];
                        $number++;
                    }
                    else
                    {
                        $card['prod_'.$number] = $iter['return'][0];
                        $number++;
                    }
                }
                else{
                    $count++;
                    if($count > 5)
                        break;
                    continue;
                }
                if($iter['balance']!="") {
                    $balance = $balance - $iter_price + (int)$iter['balance'];
                }
                if($iter['place']!="")
                {
                    if($places=="")
                    {
                        $places=$places.$iter['place'];
                    }
                    else
                    {
                        $places=$places.",".$iter['place'];
                    }
                }
                if($iter['category']!="")
                {
                    if($categories=="")
                    {
                        $categories=$categories.$iter['category'];
                    }
                    else
                    {
                        $categories=$categories.",".$iter['category'];
                    }
                }
                $count++;
                if($count > 5)
                    break;
            }
            $free = random_free($tag);
            if ($free['result'] != null && $count < 4) {
                $card['prod_'.$number] = $free['result'];
            }
            if($card!=null)
                $data[] = array('card' => $card, 'balance' => $balance);
        }
        return $data;
    } catch (Exception $e) {
        return array('msg'=>$e->getMessage());
    }
}
$app->get('/go/:price', function ($price)
{
    try {
        $data['easy']=tag_cards('easy',$price);
        $data['love']=tag_cards('love',$price);
        $data['extreme']=tag_cards('extreme',$price);
        $data['party']=tag_cards('party',$price);
        $data['places']=Place::all();
        echo json_callback($data);
    } catch (Exception $e) {
        echo json_encode(array('success' => false,'code'=>array( 'code' => 400, 'msg'=>$e->getMessage())));
    }
});

function json_callback ($data)
{
    if(isset($_GET['callback']) == "JSON_CALLBACK") {
        return 'angular.callbacks._0 (' . json_encode(array('result' => $data, 'success' => true, 'code' => '0')) . ')';
    }
    else return json_encode(array('result' => $data, 'success' => true, 'code' => '0'));
}
/*$app->get('/go/:tag/:price/:count/:rate', function ($tag,$price,$count,$rate) {
    try {
        $balance=$price;
        $i=0;
        $places="";
        $categories="";
        while($balance>100) {
            if($i<2 && $tag!='party')
            {
                $iter_price = $balance * $rate;
            }
            else {
                $iter_price = $balance;
            }
            $iter = iter($tag, $iter_price,$places,$categories);
            if($iter['msg']=="404" && $iter_price == $balance)
            {
                break;
            }
            if($iter['return']!=null) {
                $data[] = $iter['return'];
            }
            else{
                $i++;
                continue;
            }
            if($iter['balance']!="") {
                $balance = $balance - $iter_price + (int)$iter['balance'];
            }
            if($iter['place']!="")
            {
                if($places=="")
                {
                    $places=$places.$iter['place'];
                }
                else
                {
                    $places=$places.",".$iter['place'];
                }
            }
            if($iter['category']!="")
            {
                if($categories=="")
                {
                    $categories=$categories.$iter['category'];
                }
                else
                {
                    $categories=$categories.",".$iter['category'];
                }
            }
            $i++;
            if($i>$count-1)
            {
                break;
            }
        }
        $free=random_free($tag);
        if($free!=null)
        {
            $data[]=$free;
        }
        echo json_encode(array('result' => $data,'balance' => $balance, 'success' => true, 'code'=>'0'));
    } catch (Exception $e) {
        echo json_encode(array('success' => false,'code'=>array( 'code' => 400, 'msg'=>$e->getMessage())));
    }
});*/


$app->run();
?>