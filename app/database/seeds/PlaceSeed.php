﻿<?php

class PlaceSeed {

    function run()
    {
        //1
        $place = new Place;
        $place->name = "Океан";
        $place->address = "ул. Набережная, 3";
        $place->tel = "+7(423) 240-64-06";
        $place->tag = "easy,love";
        $place->description = "Кинотеатр";
        $place->photo = "http://wigg.ru/photo_places/photo1.jpg";
        $place->save();
        //2
        $place = new Place;
        $place->name = "Седьмое небо";
        $place->address = "Артем, ул. Портовая 1";
        $place->tel = "+7(924)129-0560";
        $place->tag = "extreme";
        $place->description = "Аэродром Новонежино, на территории которого осуществляются прыжки, находится в 100 км от Владивостока. Добраться до места можно на электричке или доехать на своем транспорте.";
        $place->photo = "http://wigg.ru/photo_places/photo2.jpg";
        $place->save();
        //3
        $place = new Place;
        $place->name = "LOFT";
        $place->address = "ул. Батарейная, 4";
        $place->tel = "272-38-39";
        $place->tag = "easy,love";
        $place->description = "LOFT - очередное переосмысление культуры курения кальяна.";
        $place->photo = "http://wigg.ru/photo_places/photo3.jpg";
        $place->save();
        //4
        $place = new Place;
        $place->name = "Street Bar";
        $place->address = "ул. Светланская, 83";
        $place->tel = "221-52-92";
        $place->tag = "party";
        $place->description = "Вечеринки, концерты, диджейские сейшены.";
        $place->photo = "http://wigg.ru/photo_places/photo4.jpg";
        $place->save();
        //5
        $place = new Place;
        $place->name = "Иллюзион";
        $place->address = "проспект 100-лет Владивостоку, 103";
        $place->tel = "+7 (423) 2‒406‒406";
        $place->tag = "easy,love";
        $place->description = "Кинотеатр";
        $place->photo = "http://wigg.ru/photo_places/photo5.jpg";
        $place->save();
        //6
        $place = new Place;
        $place->name = "Морской вокзал";
        $place->address = "ул. Нижнепортовая 1";
        $place->tel = "";
        $place->tag = "easy,love";
        $place->description = "Морской вокзал";
        $place->photo = "http://wigg.ru/photo_places/photo6.jpg";
        $place->save();
        //7
        $place = new Place;
        $place->name = "Coffee studio";
        $place->address = "ул. Светланская 18";
        $place->tel = "+7 (423) 2-599-227";
        $place->tag = "love,easy";
        $place->description = "Покушать и выпить кофе.";
        $place->photo = "http://wigg.ru/photo_places/photo7.jpg";
        $place->save();
        
        //8
        $place = new Place;
        $place->name = "RoalBurger";
        $place->address = "ул. Океанский проспект 111а, ул. Океаский проспект 17, ул. Батарейная 3а, пр. 100-летия Владивостока 103, пр. 100-летия Владивостока 38б, ул. Трамвайная 38б";
        $place->tel = "+7 (423) 2-752-439";
        $place->tag = "easy";
        $place->description = "Сеть ресторанов быстрого питания";
        $place->photo = "http://wigg.ru/photo_places/photo8.jpg";
        $place->save();
        
        //9
        $place = new Place;
        $place->name = "Шоколадница";
        $place->address = "ул. Светланская 13, ул. Адмирала Фокина 1а";
        $place->tel = "8(423)241-18-77, 8(423)222-26-01";
        $place->tag = "love,easy";
        $place->description = "Сеть кофеен «Шоколадница»";
        $place->photo = "http://wigg.ru/photo_places/photo9.jpg";
        $place->save();
        
        //10
        $place = new Place;
        $place->name = "iLocked";
        $place->address = "пр. Красного Знамени 59, Бестужева 21Б";
        $place->tel = "8 (423) 292-48-98";
        $place->tag = "love,easy";
        $place->description = "iLocked – это мы. Наша команда работала очень долго, чтобы собрать лучшие идеи и придумать самые необычные сюжеты для вас.";
        $place->photo = "http://wigg.ru/photo_places/photo10.jpg";
        $place->save();
        
        //11
        $place = new Place;
        $place->name = "PandoRoom";
        $place->address = "ул.Пограничная 6а";
        $place->tel = "+7 (423) 250-03-55";
        $place->tag = "love,easy";
        $place->description = "PandoRoom – Реальные квесты во Владивостоке.";
        $place->photo = "http://wigg.ru/photo_places/photo11.jpg";
        $place->save();
        
        //12
        $place = new Place;
        $place->name = "Краевой дом физкультуры";
        $place->address = "Партизанский пр-кт 2";
        $place->tel = "+7(423) 242-80-94";
        $place->tag = "easy";
        $place->description = "PandoRoom – Реальные квесты во Владивостоке.";
        $place->photo = "http://wigg.ru/photo_places/photo12.jpg";
        $place->save();
        
        //13
        $place = new Place;
        $place->name = "Cuckoo";
        $place->address = "Океанский проспект 1а ";
        $place->tel = "+7 (423) 299‒58‒58";
        $place->tag = "party";
        $place->description = "Клуб, ресторан, бар";
        $place->photo = "http://wigg.ru/photo_places/photo13.jpg";
        $place->save();
        
        //14
        $place = new Place;
        $place->name = "Центр современного искусства Заря";
        $place->address = "пр-кт 100-летия Владивостоку, 155";
        $place->tel = "+7(423) 231-71-00";
        $place->tag = "easy,love";
        $place->description = "воркшоп";
        $place->photo = "http://wigg.ru/photo_places/photo14.jpg";
        $place->save(); 
        
        //15
        $place = new Place;
        $place->name = "Библиотека им. А.М. Горького";
        $place->address = "ул. Некрасовская, 59а";
        $place->tel = "+7(423) 245-62-84";
        $place->tag = "easy,love";
        $place->description = "выставка";
        $place->photo = "http://wigg.ru/photo_places/photo15.jpg";
        $place->save(); 
        
        //16
        $place = new Place;
        $place->name = "набережная Цесаревича";
        $place->address = "Центр";
        $place->tel = "";
        $place->tag = "easy,love";
        $place->description = "Красивое и романтическое место.";
        $place->photo = "http://wigg.ru/photo_places/photo16.jpg";
        $place->save(); 
        
        //17
        $place = new Place;
        $place->name = "Экстримальный Владивосток";
        $place->address = "Владивосток";
        $place->tel = "";
        $place->tag = "extreme";
        $place->description = "Только для опытных.";
        $place->photo = "http://wigg.ru/photo_places/photo17.jpg";
        $place->save(); 
        
        //18
        $place = new Place;
        $place->name = "Тихая";
        $place->address = "Сахалинская 1";
        $place->tel = "8 800 555 35 35";
        $place->tag = "extreme";
        $place->description = "Экстримальные виды спорта";
        $place->photo = "http://wigg.ru/photo_places/photo18.jpg";
        $place->save(); 
        
        //19
        $place = new Place;
        $place->name = "Подъезд";
        $place->address = "";
        $place->tel = "";
        $place->tag = "party";
        $place->description = "Любой падик на районе";
        $place->photo = "http://wigg.ru/photo_places/photo19.jpg";
        $place->save();
        
        //20
        $place = new Place;
        $place->name = "Комета";
        $place->address = "Лазурная, 1";
        $place->tel = "8 423 255 331";
        $place->tag = "extreme";
        $place->description = "Центр зимнего отдыха";
        $place->photo = "http://wigg.ru/photo_places/photo17.jpg";
        $place->save();
        
        //21
        $place = new Place;
        $place->name = "Русский";
        $place->address = "о.Русский";
        $place->tel = "8 984 556 54 25";
        $place->tag = "extreme";
        $place->description = "Живописный остров";
        $place->photo = "http://wigg.ru/photo_places/photo17.jpg";
        $place->save();
        
        //22
        $place = new Place;
        $place->name = "Педан";
        $place->address = "Горная 105";
        $place->tel = "8 914 225 36 45";
        $place->tag = "extreme";
        $place->description = "Магическая гора Приморского края";
        $place->photo = "http://wigg.ru/photo_places/photo17.jpg";
        $place->save();
    }
}