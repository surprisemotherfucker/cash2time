<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class ProductMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('products');
        Capsule::schema()->create('products', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('parent');
            $table->integer('price');
            $table->string('start_time');
            $table->string('end_time');
            $table->text('description');
            $table->integer('priority');
            $table->string('category');
            $table->string('photo');
            $table->timestamps();
        });
    }
}
