    <?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Example migration for use with "novice"
 */
class PlaceMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('places');
        Capsule::schema()->create('places', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('tel');
            $table->string('tag');
            $table->text('description');
            $table->string('photo'); 
            $table->timestamps();
        });
    }
}
