<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Place extends Eloquent {

    public function product()
    {
        return $this->hasMany('Product');
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
}
